package com.kryviak;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverManager {

    private static AndroidDriver<MobileElement> driver;

    private static DesiredCapabilities caps() {
        DesiredCapabilities caps = new DesiredCapabilities();
        File app = new File("C:\\work\\projects\\TaskAndroidAppium\\Gmail.apk");
        caps.setCapability(MobileCapabilityType.APP, app);
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1.2");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Redmi");
        caps.setCapability(MobileCapabilityType.UDID, "992d3b27d15");
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 30);
        caps.setCapability("appPackage", "com.google.android.gm");
        caps.setCapability("appActivity", ".ConversationListActivityGmail");
        return caps;
    }

    public static AndroidDriver getDriver() {
        if (driver == null) {
            try {
                driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), caps());
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return driver;
    }

    void uninstallApp() {
        driver.removeApp("com.example.android.contactmanager");
    }
}