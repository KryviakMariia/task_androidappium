package com.kryviak.bo;

import com.kryviak.pages.HomePage;
import com.kryviak.pages.SendPage;
import com.kryviak.pages.SentPage;

public class SendBO {

    private HomePage homePage = new HomePage();
    private SendPage sendPage = new SendPage();
    private SentPage sentPage = new SentPage();

    public void sendMessage(String email, String subject) {
        homePage.goToMyPage();
        homePage.choseMyAccount();
        homePage.clickNext();
        homePage.clickNext2();
        homePage.createNewMessage();
        sendPage.chooseYourEmail();
        sendPage.setEmailTo(email);
        sendPage.setSubjectTo(subject);
        sendPage.clickSendMessage();
    }

    public String checkIfMessageSent() {
        sentPage.clickClickToViewAllSentMessage();
        sentPage.clickSent();
        sentPage.chooseSentMessage();
        return sentPage.lastSentMessage();
    }
}
