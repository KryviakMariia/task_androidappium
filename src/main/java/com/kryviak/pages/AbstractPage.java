package com.kryviak.pages;

import com.kryviak.DriverManager;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

abstract class AbstractPage {
    AbstractPage() {
        AndroidDriver driver = DriverManager.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver, 40, TimeUnit.SECONDS), this);
    }
}
