package com.kryviak.pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePage extends AbstractPage {

    @AndroidFindBy(id = "com.google.android.gm:id/welcome_tour_got_it")
    private MobileElement gotoMyPageButton;

    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement choseMyAccountButton;

    @AndroidFindBy(id = "com.google.android.gm:id/gm_dismiss_button")
    private MobileElement nextButton;

    @AndroidFindBy(id = "com.google.android.gm:id/gm_dismiss_button")
    private MobileElement nextButton2;

    @AndroidFindBy(id = "com.google.android.gm:id/compose_button")
    private MobileElement createNewMessageButton;

    @AndroidFindBy(id = "com.google.android.gm:id/open_search_bar_text_view")
    private MobileElement searchTextField;

    public void createNewMessage() {
        createNewMessageButton.click();
    }

    public void goToMyPage() {
        gotoMyPageButton.click();
    }

    public void choseMyAccount() {
        choseMyAccountButton.click();
    }

    public void clickNext() {
        nextButton.click();
    }

    public void clickNext2() {
        nextButton2.click();
    }
}
