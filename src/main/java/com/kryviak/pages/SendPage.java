package com.kryviak.pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class SendPage  extends AbstractPage {

    @AndroidFindBy(id = "com.google.android.gm:id/to")
    private MobileElement emailToTextField;

    @AndroidFindBy(id = "com.google.android.gm:id/subject")
    private MobileElement subjectToTextField;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v7.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")
    private MobileElement chooseYourEmailButton;

    @AndroidFindBy(id = "com.google.android.gm:id/send")
    private MobileElement sentMessageButton;

    public void setEmailTo(String emailTo){
        emailToTextField.sendKeys(emailTo);
    }

    public void setSubjectTo(String subjectTo){
        subjectToTextField.click();
        subjectToTextField.sendKeys(subjectTo);
    }

    public void chooseYourEmail(){
        chooseYourEmailButton.click();
    }

    public void clickSendMessage(){
        sentMessageButton.click();
    }
}
