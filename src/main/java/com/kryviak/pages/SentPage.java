package com.kryviak.pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class SentPage extends AbstractPage{

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Open navigation drawer\"]")
    private MobileElement clickToViewAllSentMessageButton;


    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout[2]/android.widget.ListView/android.widget.LinearLayout[10]")
    private MobileElement clickSentButton;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.view.View[1]")
    private MobileElement chooseSentMessageButton;

    @AndroidFindBy(id = "com.google.android.gm:id/subject_and_folder_view")
    private MobileElement lastSentMessageLabel;


    public void clickClickToViewAllSentMessage(){
        clickToViewAllSentMessageButton.click();
    }

    public void clickSent(){
        clickSentButton.click();
    }

    public void chooseSentMessage(){
        chooseSentMessageButton.click();
    }

    public String lastSentMessage(){
        return lastSentMessageLabel.getText();
    }
}
