package com.kryviak;

import com.kryviak.bo.SendBO;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class GmailTest {

    private DriverManager driverManager = new DriverManager();

    @Test
    public void testSendMessage() {
        SendBO sendBO = new SendBO();
        sendBO.sendMessage("kryviakmaria@gmail.com", "Hello");
        Assert.assertEquals(sendBO.checkIfMessageSent(), "Hello Add label ", "Label should be 'Hello Add label'");
    }

    @AfterMethod
    public void closeBrowser() {
        driverManager.uninstallApp();
    }
}
